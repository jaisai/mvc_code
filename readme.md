Jaswanth Kumar Jonnalagadda
Graduate Student |NWMSU
Graduate Assistant - NS	
Web Master| ACM-G
Ph.: (484) 302 9172
Email: jaswanthkumar54321@gmail.com

The Recruiter, 
9th Networks.

 I am Jaswanth Kumar Jonnalagadda pursuing master’s degree at Northwest Missouri State University in Applied Computer Sciences. I am writing this in response to the Entry level Network Engineer position with 9th Networks.
Prior joining Northwest Missouri State University I worked at TATA Consultancy Services from June 2015 to August 2016 as Assistant Systems Engineer, managed client’s Database to support continuous production. As a daily routine, I came across different issues from various trading partners to maintain the continuous production and safeguarding the database from crashing. Apart from database development and supporting me and my team developed a simple web application that logs all the issues from various trading partners automatically to be used as a reference. We developed this web application using HTML, CSS, JavaScript and we used Apache tomcat to host our application.
My team is also responsible for improving the performance of the database by writing efficient queries and redesigning the index positions and also writing efficient triggers to retrieve date more precise and faster.
Being a graduate Student at Northwest Missouri State University I had the opportunity to develop a distributed online application, Job Cost Estimator (JCE) for Mr. Curt Carpenter of C&G Coatings (Owner). This web application takes data from the US home depot in JSON format and calculates the no. of days and people required to complete the task in a most profitable way. We developed this application using HTML, CSS, JavaScript, and MongoDB. We used Tomcat server to host the application. 

As a graduate student, I got the opportunity to work as software developer for our university CITE office. Here I and my team developed a web application for the school of education to track their assessments. We used HTML5, JSP, CSS3, and JavaScript to develop all the client side application (front end). We used MySQL as our database server, we implemented JDBC to connect the web application to the database server. Hosted the application in our university Glassfish server running over Linux environment. This application eased the life of all the students’ professors to keep
track of their assignments and never miss one.

I also worked as Graduate & Research Assistant for Network Fundamentals & Security course with Dr. Richard Scott Bell as our instructor. I and Dr. Bell worked closely in analyzing various security flaws in mobile & pc operating systems. Being graduate assistant I design & conduct labs for students over various tools and techniques that are required to keep themselves from cyber-attacks.

Apart from academics, I work for ACM-G an organization at Northwest, which was initiated to help graduate students in every possible way to complete their graduation and be successful. I also work as Publicist for ISA (Indian Student Organization) a group that was organized to help the Indian students and enlightening them about various aspects of their new home. 
 	
With my relevant skills and knowledge, I can be a good team player and work with full attention. I would welcome the opportunity and can assure you that I will fulfill the task with utmost discipline and dedication.

I appreciate your time in considering and reviewing my application.

Sincerely, 
Jaswanth Kumar Jonnalagadda.
